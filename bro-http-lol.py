"""
"""

import phantom.rules as phantom
import json
from datetime import datetime, timedelta

def on_start(container):
    phantom.debug('on_start() called')
    
    # call 'url_filter' block
    url_filter(container=container)

    return

def url_filter(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None):
    phantom.debug('url_filter() called')

    # collect filtered artifact ids for 'if' condition 1
    matched_artifacts_1, matched_results_1 = phantom.condition(
        container=container,
        conditions=[
            ["artifact:*.cef.destinationHostName", "!=", ""],
        ],
        name="url_filter:condition_1")

    # call connected blocks if filtered artifacts or results
    if matched_artifacts_1 or matched_results_1:
        msg_format(action=action, success=success, container=container, results=results, handle=handle, filtered_artifacts=matched_artifacts_1, filtered_results=matched_results_1)

    return

def msg_format(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None):
    phantom.debug('msg_format() called')
    
    template = """Source IP: {0}
Server Name: {1}
Subject: {2}
Version: {3}"""

    # parameter list for template variable replacement
    parameters = [
        "filtered-data:url_filter:condition_1:artifact:*.cef.sourceAddress",
        "filtered-data:url_filter:condition_1:artifact:*.cef.destinationHostName",
        "filtered-data:url_filter:condition_1:artifact:*.subject",
        "filtered-data:url_filter:condition_1:artifact:*.version",
    ]

    phantom.format(container=container, template=template, parameters=parameters, name="msg_format")

    send_message_2(container=container)

    return

def send_message_2(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None):
    phantom.debug('send_message_2() called')

    # collect data for 'send_message_2' call
    formatted_data_1 = phantom.get_format_data(name='msg_format')

    parameters = []
    
    # build parameters list for 'send_message_2' call
    parameters.append({
        'message': formatted_data_1,
        'destination': "#phantom",
    })

    phantom.act("send message", parameters=parameters, assets=['mr_x'], name="send_message_2")

    return

def on_finish(container, summary):
    phantom.debug('on_finish() called')
    # This function is called after all actions are completed.
    # summary of all the action and/or all detals of actions 
    # can be collected here.

    # summary_json = phantom.get_summary()
    # if 'result' in summary_json:
        # for action_result in summary_json['result']:
            # if 'action_run_id' in action_result:
                # action_results = phantom.get_action_results(action_run_id=action_result['action_run_id'], result_data=False, flatten=False)
                # phantom.debug(action_results)

    return